use anyhow::Error;
use bincode::{deserialize_from, serialize_into};
use clap::{App, Arg};
use contract::{Identity, VLCCommand};
use dashmap::DashMap;
use std::io;
use std::sync::{
    atomic::{AtomicU64, Ordering::Relaxed},
    Arc,
};
use tokio::net::{TcpListener, TcpStream};

#[derive(Debug)]
pub struct Server {
    id: AtomicU64,
    clients: DashMap<u64, std::net::TcpStream>,
}

impl Server {
    pub fn new() -> Server {
        Server {
            id: AtomicU64::new(0),
            clients: DashMap::new(),
        }
    }
}

async fn on_clients_connect(server: Arc<Server>, socket: TcpStream) -> Result<(), Error> {
    socket.readable().await?;
    let mut std_socket = socket.into_std().unwrap();
    std_socket.set_nonblocking(false).unwrap();
    let id = deserialize_from(&mut std_socket)?;
    //TODO: don't println!, log
    println!("Got identity {:?}", id);
    match id {
        Identity::Client => {
            let id = server.id.fetch_add(1, Relaxed);
            server.clients.insert(id, std_socket);
            Ok(())
        }
        Identity::Host => loop {
            let command: VLCCommand = deserialize_from(&mut std_socket)?;
            //TODO: don't println!, log
            println!("Host send command: {:?}", command);
            for conn in server.clients.iter() {
                // Ignore error so as not to crash the server due to one bad client
                //TODO: log
                let _ = serialize_into(&mut conn.value(), &command);
            }
        },
    }
}

pub const APP_NAME: &str = "server";
pub const APP_VERSION: &str = "0.0.1";
pub const APP_AUTHOR: &str =
    "Qingyuan Qie <will.qie@mail.utoronto.ca> and Jad Ghalayini <jad.ghalayini@alum.utoronto.ca>";

#[tokio::main]
async fn main() -> io::Result<()> {
    let matches = App::new(APP_NAME)
        .version(APP_VERSION)
        .author(APP_AUTHOR)
        .arg(Arg::with_name("url").short("u").takes_value(true))
        .get_matches();

    let url = matches.value_of("url").unwrap_or("127.0.0.1:8848");

    //TODO: proper logging
    println!("Listening at {}", url);

    let server = Arc::new(Server::new());
    let listener = TcpListener::bind(url).await?;

    loop {
        match listener.accept().await {
            Ok((socket, addr)) => {
                //TODO: proper logging
                println!("Accepting connection from {:?}", addr);
                let this_server = server.clone();
                tokio::spawn(async move {
                    on_clients_connect(this_server, socket).await.unwrap();
                });
            }
            Err(e) => println!("Error accepting connecting client {:?}", e),
        }
    }
}
