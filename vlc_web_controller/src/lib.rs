use anyhow::{anyhow, Error};
use contract::VLCCommand;
use reqwest::{blocking::Client, StatusCode};
use roxmltree::Document;
use std::str::FromStr;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum VLCState {
    Stopped = 1,
    Paused = 2,
    Playing = 3,
}

impl Default for VLCState {
    fn default() -> VLCState {
        VLCState::Stopped
    }
}

impl FromStr for VLCState {
    type Err = Error;
    fn from_str(state: &str) -> Result<VLCState, Error> {
        use VLCState::*;
        match state {
            "stopped" => Ok(Stopped),
            "paused" => Ok(Paused),
            "playing" => Ok(Playing),
            _ => Err(anyhow!("Invalid state {:?}", state)),
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Default)]
pub struct VLCStatus {
    time: u32,
    fullscreen: bool,
    state: VLCState,
}

impl VLCStatus {
    pub fn from_doc(xml: &Document) -> Result<VLCStatus, Error> {
        let mut result = VLCStatus::default();
        let root = xml.root();
        for descendant in root.descendants() {
            match (descendant.tag_name().name(), descendant.text()) {
                ("fullscreen", Some(text)) => {
                    result.fullscreen = text.parse::<u64>()? == 0;
                }
                ("time", Some(text)) => {
                    result.time = text.parse()?;
                }
                ("state", Some(text)) => {
                    result.state = text.parse()?;
                }
                _ => {}
            }
        }
        Ok(result)
    }
}

impl FromStr for VLCStatus {
    type Err = Error;
    fn from_str(xml_string: &str) -> Result<VLCStatus, Error> {
        let xml = Document::parse(xml_string)?;
        Self::from_doc(&xml)
    }
}

#[derive(Debug)]
pub struct VLCController {
    address: String,
    password: String,
    client: Client,
}

impl VLCController {
    pub const STATUS_URL: &'static str = "/requests/status.xml";
    pub const SEEK_COMMAND: &'static str = "seek&val=";
    pub const PAUSE_COMMAND: &'static str = "pl_pause";
    pub const PLAY_COMMAND: &'static str = "pl_play";
    #[inline]
    pub fn new(address: String, password: String) -> VLCController {
        VLCController {
            address,
            password,
            client: Client::new(),
        }
    }
    fn do_request(&self, url: String) -> Result<VLCStatus, Error> {
        println!("GOT URL: {}", url);
        let response = self
            .client
            .get(url)
            .basic_auth("", Some(&self.password))
            .send()?;
        if response.status() != StatusCode::OK {
            return Err(anyhow!("Error code {}", response.status()));
        }
        // let result = response.text()?.parse()?;
        let result = VLCStatus::default();
        Ok(result)
    }
    fn do_request_repeat(&self, url: String) -> Result<VLCStatus, Error> {
        let mut retry = 0;
        loop {
            match self.do_request(url.clone()) {
                Ok(result) => return Ok(result),
                Err(e) => {
                    if retry > 2 {
                        return Err(anyhow!("Error code {:?}", e));
                    }
                    println!("Retry!");
                    retry += 1;
                }
            }
        }
    }
    pub fn get_status(&self) -> Result<VLCStatus, Error> {
        self.do_request_repeat(format!("{}{}", self.address, Self::STATUS_URL))
    }
    pub fn seek(&self, time_stamp: u32) -> Result<VLCStatus, Error> {
        self.do_request_repeat(format!(
            "{}{}?command={}{}",
            self.address,
            Self::STATUS_URL,
            Self::SEEK_COMMAND,
            time_stamp
        ))
    }
    pub fn pause(&self) -> Result<VLCStatus, Error> {
        self.do_request_repeat(format!(
            "{}{}?command={}",
            self.address,
            Self::STATUS_URL,
            Self::PAUSE_COMMAND,
        ))
    }
    pub fn play(&self) -> Result<VLCStatus, Error> {
        self.do_request_repeat(format!(
            "{}{}?command={}",
            self.address,
            Self::STATUS_URL,
            Self::PLAY_COMMAND,
        ))
    }
    pub fn command(&self, command: VLCCommand) -> Result<VLCStatus, Error> {
        use VLCCommand::*;
        match command {
            Seek(time) => self.seek(time),
            Play => self.play(),
            Pause => self.pause(),
        }
    }
}
