use anyhow::Error;
use bincode::serialize_into;
use clap::{App, Arg};
use contract::{Identity, VLCCommand};
use rustyline::Editor;
use std::net::TcpStream;

pub const APP_NAME: &str = "host";
pub const APP_VERSION: &str = "0.0.1";
pub const APP_AUTHOR: &str =
    "Qingyuan Qie <will.qie@mail.utoronto.ca> and Jad Ghalayini <jad.ghalayini@alum.utoronto.ca>";

fn main() -> Result<(), Error> {
    let matches = App::new(APP_NAME)
        .version(APP_VERSION)
        .author(APP_AUTHOR)
        .arg(Arg::with_name("url").short("u").takes_value(true))
        .get_matches();

    let url = matches.value_of("url").unwrap_or("127.0.0.1:8848");

    // Connect to a peer
    let mut stream = TcpStream::connect(url).unwrap();
    serialize_into(&mut stream, &Identity::Host)?;
    // Get an editor
    let mut editor = Editor::<()>::new();
    loop {
        let command: VLCCommand = editor.readline(">>> ")?.parse()?;
        serialize_into(&mut stream, &command)?;
    }
}
