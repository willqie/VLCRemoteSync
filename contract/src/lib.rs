use anyhow::{anyhow, Error};
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{digit1, multispace0, multispace1},
    combinator::*,
    sequence::*,
    IResult,
};
use serde::{Deserialize, Serialize};
use std::str::FromStr;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub enum Identity {
    Host = 0,
    Client = 1,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub enum VLCCommand {
    Seek(u32),
    Pause,
    Play,
}

impl VLCCommand {
    pub fn parse(input: &str) -> IResult<&str, VLCCommand> {
        alt((
            map_res(
                preceded(pair(tag("seek"), multispace1), digit1),
                |digit: &str| digit.parse().map(VLCCommand::Seek),
            ),
            map(tag("pause"), |_| VLCCommand::Pause),
            map(tag("play"), |_| VLCCommand::Play),
        ))(input)
    }
}

impl FromStr for VLCCommand {
    type Err = Error;
    fn from_str(input: &str) -> Result<VLCCommand, Error> {
        let (rest, command) =
            terminated(VLCCommand::parse, multispace0)(input).map_err(|err| anyhow!("{}", err))?;
        if rest != "" {
            Err(anyhow!(
                "Incompletely parsed input, rest = {:?}",
                rest.to_owned()
            ))
        } else {
            Ok(command)
        }
    }
}

impl Into<u8> for Identity {
    fn into(self) -> u8 {
        self as u8
    }
}
