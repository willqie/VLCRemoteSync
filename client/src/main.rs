use anyhow::Error;
use bincode::{deserialize_from, serialize_into};
use clap::{App, Arg};
use contract::Identity;
use std::net::TcpStream;
use vlc_web_controller::VLCController;

pub const APP_NAME: &str = "client";
pub const APP_VERSION: &str = "0.0.1";
pub const APP_AUTHOR: &str =
    "Qingyuan Qie <will.qie@mail.utoronto.ca> and Jad Ghalayini <jad.ghalayini@alum.utoronto.ca>";

fn main() -> Result<(), Error> {
    let matches = App::new(APP_NAME)
        .version(APP_VERSION)
        .author(APP_AUTHOR)
        .arg(Arg::with_name("url").short("u").takes_value(true))
        .arg(Arg::with_name("vlc").short("v").takes_value(true))
        .arg(Arg::with_name("password").short("p").takes_value(true))
        .get_matches();

    let url = matches.value_of("url").unwrap_or("127.0.0.1:8848");
    let vlc = matches.value_of("vlc").unwrap_or("http://127.0.0.1:8080");
    let password = matches.value_of("password").unwrap_or("1234");

    let mut stream = TcpStream::connect(url)?;
    let controller = VLCController::new(vlc.to_owned(), password.to_owned());

    serialize_into(&mut stream, &Identity::Client)?;
    loop {
        let msg = deserialize_from(&mut stream)?;
        println!("{:?}", msg);
        controller.command(msg)?;
    }
}
